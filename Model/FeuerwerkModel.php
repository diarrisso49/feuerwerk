<?php

class FeuerwerkModel
{
    private $name;
    private $anzahl;
    private $dauer;
    private $art;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAnzahl()
    {
        return $this->anzahl;
    }

    /**
     * @param mixed $anzahl
     */
    public function setAnzahl($anzahl)
    {
        $this->anzahl = $anzahl;
    }

    /**
     * @return mixed
     */
    public function getDauer()
    {
        return $this->dauer;
    }

    /**
     * @param mixed $dauer
     */
    public function setDauer($dauer)
    {
        $this->dauer = $dauer;
    }

    /**
     * @return mixed
     */
    public function getArt()
    {
        return $this->art;
    }

    /**
     * @param mixed $art
     */
    public function setArt($art)
    {
        $this->art = $art;
    }




}